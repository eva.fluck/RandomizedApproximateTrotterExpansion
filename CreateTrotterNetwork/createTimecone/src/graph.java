import java.io.*;
import java.util.Random;
//import java.math.*;

public class graph {
	private boolean[] stored;
	private int observable;
	private int pos;
	private int size;
	private int time;
	private int[] numberInnerNodes;
	private int modes; //total number of different modes
	private int[][][] innerNodes; //array of id of inner nodes, 0 means node does not exists in this mode, corresponding adjungiert node has id+1
									//input bits have ids from numberInnerNodes +1 to +4*size (where again adjungiert is +1)
									// observable has id numberInnerNodes+4*size+1
	private int[][][][] nodesToEdges; //array content: matrix of arrays {node time, node position, {left input edge, right input edge, left output edge, right output edge}}
	private int[][][][] nodesToNodes; //array content: matrix of arrays {node time, node position, {left input node, right input node, left output node, right output node}}
	private int[][] inputEdges; //array content 0..2*size-1 input bits, 2*size..4*size-1 adjungiert input bits connected to edgesID
	private int[][] inputToNodes; // array content 0..2*size-1 input bits, 2*size..4*size-1 adjungiert input bits connected to nodesID
	private int[][][] edges; //array content: array of arrays {edge number, {input time, input position, output time, output position}}
	private int[] numberEdges; // array of the total number of edges per mode
	private int[] round; //for each mode store how many graphs of this mode have been created
	

	public int getObservable() {
		return observable;
	}
	public int getSize() {
		return size;
	}
	
	
	public graph(int size, int time, int observable) {
		super();
		this.modes = 6; //total number of different modes 0 is the normal graph
						//1 is nodes structually deleted
						//2 is nodes uniformly at random deleted (but the horizon) s.t. log(input) nodes remain
						//3 is nodes uniformly at random deletes s.t. input*log(input) nodes remain
						//4 in nodes uniformly at random deleted within one row s.t. log(input) nodes remain per row
						//5 is nodes uniformly at random deleted within every second row s.t. log(input) nodes remain per row
						//6 is nodes structually deleted every second row
		this.round = new int[modes];
		this.stored = new boolean[modes];
		for (int mode=1;mode<modes;mode++){
			this.stored[mode] = true;
			this.round[mode] = 0;
		}
		this.stored[0]=false;
		this.observable = observable;
		this.pos = observable-1; //position of observable
		if ((observable % 2)!=0){
			if (observable > 1){
				this.pos = observable -2;
			}
		}
		else if (observable == 2*size){
			this.pos = 2*size-2;
		}
		this.size = size;
		this.time = time;
		this.innerNodes = new int[modes][time][2*size-1];
		this.nodesToEdges = new int[modes][2*time][2*size-1][4];
		this.nodesToNodes = new int[modes][2*time][2*size-1][4];
		this.inputEdges = new int[modes][4*size];
		this.inputToNodes = new int[modes][4*size];
		this.edges = new int[modes][][];
		for(int t=0;t<time;t++){
			for(int m=0;m<2*size-1;m++){
				innerNodes[0][t][m] = 0;
			}
		}
		this.numberInnerNodes = new int[modes];
		this.numberEdges = new int[modes];
		this.numberInnerNodes[0] = 0;
		// create grid in time cone
		for(int t=0;t<time;t++){
			for(int m=1;m<2*size-2;m = m+2){
				if(withinHorizon(m,t)){//still within time cone
					innerNodes[0][t][m]=numberInnerNodes[0]+1;
					numberInnerNodes[0] += 2;
				}
			}
			for(int m=0;m<2*size-1;m = m+2){
				if(withinHorizon(m,t)){// still within time cone
					innerNodes[0][t][m]=numberInnerNodes[0]+1;
					numberInnerNodes[0] += 2;
				}
			}
		}
		createEdges(0);
	}
	
	
	/*
	 * this function will thin out the graph using the method set by the mode
	 * @param startMode is the lowest mode for which to start deleting
	 * 	0 means normal graph
	 * 	1 means delete inner nodes in a structured deterministic way
	 * 	2 means delete inner nodes uniformly at random with probability p=log(n)/n
	 * 	3 is nodes uniformly at random deletes s.t. input*log(input) nodes remain
	 * 	4 in nodes uniformly at random deleted within one row s.t. log(input) nodes remain per row
	 * 	5 is nodes uniformly at random deleted within every second row s.t. log(input) nodes remain per row
	 */
	public void createModes(int startMode){
		for (int mode = startMode;mode<modes;mode++){
			if (stored[mode]){
				for(int t=0;t<time;t++){
					for(int m=0;m<2*size-1;m++){
						innerNodes[mode][t][m] = innerNodes[0][t][m];
					}
				}
				deleteNodes(mode);
				stored[mode]=false;
				createEdges(mode);
			}
			else{
				System.out.println("You tried to delete nodes for a mode where an instance exists, that has not been stored. Mode was: "+Integer.toString(mode));
			}
		}
	}

	/*
	 * this function creates the association of edges to nodes and finds the connection between nodes
	 * @param mode is the mode for which to do this process
	 */
	private void createEdges(int mode){
		numberEdges[mode] = numberInnerNodes[mode]*2+2*size+1; //edges=sum(vertex degree)/2
		int[] nextNode;
		int[] thisNode = new int[2];
		int edgeCounter = 0;
		int innerNodeCounter = 0;
		edges[mode] = new int[numberEdges[mode]][4];
		for (int m=0;m<2*size;m++){ //input edges
			thisNode[0] = -1;
			thisNode[1] = m;
			nextNode = findOutput(thisNode,'l',mode);
			inputEdges[mode][2*size + thisNode[1]]=edgeCounter;
			inputToNodes[mode][2*size + thisNode[1]]=innerNodes[mode][nextNode[0]][nextNode[1]]+1;
			nodesToEdges[mode][time + nextNode[0]][nextNode[1]][nextNode[2]]=edgeCounter;
			nodesToNodes[mode][time + nextNode[0]][nextNode[1]][nextNode[2]]=numberInnerNodes[mode]+1+2*thisNode[1]+1;
			edges[mode][edgeCounter][0] = -1;
			edges[mode][edgeCounter][1] = 2*size + thisNode[1];
			edges[mode][edgeCounter][2] = time + nextNode[0];
			edges[mode][edgeCounter][3] = nextNode[1];
			edgeCounter++;
			inputEdges[mode][thisNode[1]]=edgeCounter;
			inputToNodes[mode][thisNode[1]]=innerNodes[mode][nextNode[0]][nextNode[1]];
			nodesToEdges[mode][nextNode[0]][nextNode[1]][nextNode[2]]=edgeCounter;
			nodesToNodes[mode][nextNode[0]][nextNode[1]][nextNode[2]]=numberInnerNodes[mode]+1+2*thisNode[1];
			edges[mode][edgeCounter][0] = -1;
			edges[mode][edgeCounter][1] = thisNode[1];
			edges[mode][edgeCounter][2] = nextNode[0];
			edges[mode][edgeCounter][3] = nextNode[1];
			edgeCounter++;
		}
		for (int t=0;t<time;t++){
			for (int m=0;m<2*size-1;m++){
				if(innerNodes[mode][t][m]!=0){
					innerNodeCounter++;
					innerNodeCounter++;
					thisNode[0] = t;
					thisNode[1] = m;
					nextNode = findOutput(thisNode,'l',mode); // next neighbor on left output
					if(nextNode[0]<0){ //jump over to adjungiert
						nextNode[0] = time-nextNode[0]-1; // go from negative time to the range time to 2*time -1
						nodesToEdges[mode][thisNode[0]][thisNode[1]][2]=edgeCounter;
						nodesToNodes[mode][thisNode[0]][thisNode[1]][2]=innerNodes[mode][thisNode[0]][thisNode[1]]+1;
						nodesToEdges[mode][nextNode[0]][nextNode[1]][2]=edgeCounter; // jump over to adjungiert means output is connected to output
						nodesToNodes[mode][nextNode[0]][nextNode[1]][2]=innerNodes[mode][thisNode[0]][thisNode[1]];
						edges[mode][edgeCounter][0] = thisNode[0];
						edges[mode][edgeCounter][1] = thisNode[1];
						edges[mode][edgeCounter][2] = nextNode[0];
						edges[mode][edgeCounter][3] = nextNode[1];
						edgeCounter++;
					}
					else if(nextNode[1]==-observable){ //next neighbor is the observable, note neighbor of edge to observable is always +1
						nodesToEdges[mode][thisNode[0]][thisNode[1]][2]=edgeCounter;
						nodesToNodes[mode][thisNode[0]][thisNode[1]][2]=numberInnerNodes[mode]+4*size+1;
						edges[mode][edgeCounter][0] = thisNode[0];
						edges[mode][edgeCounter][1] = thisNode[1];
						edges[mode][edgeCounter][2] = nextNode[0];
						edges[mode][edgeCounter][3] = nextNode[1];
						edgeCounter++;
						nodesToEdges[mode][time + thisNode[0]][thisNode[1]][2]=edgeCounter;
						nodesToNodes[mode][time + thisNode[0]][thisNode[1]][2]=numberInnerNodes[mode]+4*size+1;
						edges[mode][edgeCounter][2] = time + thisNode[0];
						edges[mode][edgeCounter][3] = thisNode[1];
						edges[mode][edgeCounter][0] = time + nextNode[0];
						edges[mode][edgeCounter][1] = nextNode[1];
						edgeCounter++;
					}
					else { // no jump to adjungiert, thus we have to copy the pattern
						nodesToEdges[mode][time + thisNode[0]][thisNode[1]][2]=edgeCounter;
						nodesToNodes[mode][time + thisNode[0]][thisNode[1]][2]=innerNodes[mode][nextNode[0]][nextNode[1]]+1;
						nodesToEdges[mode][time + nextNode[0]][nextNode[1]][nextNode[2]]=edgeCounter;
						nodesToNodes[mode][time + nextNode[0]][nextNode[1]][nextNode[2]]=innerNodes[mode][thisNode[0]][thisNode[1]]+1;
						edges[mode][edgeCounter][0] = time + thisNode[0];
						edges[mode][edgeCounter][1] = thisNode[1];
						edges[mode][edgeCounter][2] = time + nextNode[0];
						edges[mode][edgeCounter][3] = nextNode[1];
						edgeCounter++;
						nodesToEdges[mode][thisNode[0]][thisNode[1]][2]=edgeCounter;
						nodesToNodes[mode][thisNode[0]][thisNode[1]][2]=innerNodes[mode][nextNode[0]][nextNode[1]];
						nodesToEdges[mode][nextNode[0]][nextNode[1]][nextNode[2]]=edgeCounter;
						nodesToNodes[mode][nextNode[0]][nextNode[1]][nextNode[2]]=innerNodes[mode][thisNode[0]][thisNode[1]];
						edges[mode][edgeCounter][0] = thisNode[0];
						edges[mode][edgeCounter][1] = thisNode[1];
						edges[mode][edgeCounter][2] = nextNode[0];
						edges[mode][edgeCounter][3] = nextNode[1];
						edgeCounter++;
					}
					
					nextNode = findOutput(thisNode,'r',mode); //next neigbor on right output
					if(nextNode[0]<0){ //jump over to adjungiert
						nextNode[0] = time-nextNode[0]-1; // go from negative time to the range time to 2*time -1
						nodesToEdges[mode][thisNode[0]][thisNode[1]][3]=edgeCounter;
						nodesToNodes[mode][thisNode[0]][thisNode[1]][3]=innerNodes[mode][thisNode[0]][thisNode[1]]+1;
						nodesToEdges[mode][nextNode[0]][nextNode[1]][3]=edgeCounter; // jump over to adjungiert means output is connected to output
						nodesToNodes[mode][nextNode[0]][nextNode[1]][3]=innerNodes[mode][thisNode[0]][thisNode[1]];
						edges[mode][edgeCounter][0] = thisNode[0];
						edges[mode][edgeCounter][1] = thisNode[1];
						edges[mode][edgeCounter][2] = nextNode[0];
						edges[mode][edgeCounter][3] = nextNode[1];
						edgeCounter++;
					}
					else if(nextNode[1]==-observable){ //next neighbor is the observable, note neighbor of edge to observable is always +1
						nodesToEdges[mode][thisNode[0]][thisNode[1]][3]=edgeCounter;
						nodesToNodes[mode][thisNode[0]][thisNode[1]][3]=numberInnerNodes[mode]+4*size+1;
						edges[mode][edgeCounter][0] = thisNode[0];
						edges[mode][edgeCounter][1] = thisNode[1];
						edges[mode][edgeCounter][2] = nextNode[0];
						edges[mode][edgeCounter][3] = nextNode[1];
						edgeCounter++;
						nodesToEdges[mode][time + thisNode[0]][thisNode[1]][3]=edgeCounter;
						nodesToNodes[mode][time + thisNode[0]][thisNode[1]][3]=numberInnerNodes[mode]+4*size+1;
						edges[mode][edgeCounter][2] = time + thisNode[0];
						edges[mode][edgeCounter][3] = thisNode[1];
						edges[mode][edgeCounter][0] = time + nextNode[0];
						edges[mode][edgeCounter][1] = nextNode[1];
						edgeCounter++;
					}
					else { // no jump to adjungiert, thus we have to copy the pattern
						nodesToEdges[mode][time + thisNode[0]][thisNode[1]][3]=edgeCounter;
						nodesToNodes[mode][time + thisNode[0]][thisNode[1]][3]=innerNodes[mode][nextNode[0]][nextNode[1]]+1;
						nodesToEdges[mode][time + nextNode[0]][nextNode[1]][nextNode[2]]=edgeCounter;
						nodesToNodes[mode][time + nextNode[0]][nextNode[1]][nextNode[2]]=innerNodes[mode][thisNode[0]][thisNode[1]]+1;
						edges[mode][edgeCounter][0] = time + thisNode[0];
						edges[mode][edgeCounter][1] = thisNode[1];
						edges[mode][edgeCounter][2] = time + nextNode[0];
						edges[mode][edgeCounter][3] = nextNode[1];
						edgeCounter++;
						nodesToEdges[mode][thisNode[0]][thisNode[1]][3]=edgeCounter;
						nodesToNodes[mode][thisNode[0]][thisNode[1]][3]=innerNodes[mode][nextNode[0]][nextNode[1]];
						nodesToEdges[mode][nextNode[0]][nextNode[1]][nextNode[2]]=edgeCounter;
						nodesToNodes[mode][nextNode[0]][nextNode[1]][nextNode[2]]=innerNodes[mode][thisNode[0]][thisNode[1]];
						edges[mode][edgeCounter][0] = thisNode[0];
						edges[mode][edgeCounter][1] = thisNode[1];
						edges[mode][edgeCounter][2] = nextNode[0];
						edges[mode][edgeCounter][3] = nextNode[1];
						edgeCounter++;
					}
				}
			}
		}
		if (numberEdges[mode]!=edgeCounter){
			System.out.println("edges misscounted in mode " + Integer.toString(mode));
			System.out.println("expected " +Integer.toString(numberEdges[mode]) + " but found " + Integer.toString(edgeCounter));
			System.out.println("expected inner Nodes " + numberInnerNodes[mode] + " but found " + Integer.toString(innerNodeCounter));
		}
	}
	
	/*
	 * this function deletes nodes for the given mode
	 * @param mode is the mode of deletion
	 * 	0 means normal graph
	 * 	1 means delete inner nodes in a structured deterministic way
	 * 	2 means delete inner nodes uniformly at random with probability p=log(n)/n
	 * 	3 is nodes uniformly at random deletes s.t. input*log(input) nodes remain
	 * 	4 in nodes uniformly at random deleted within one row s.t. log(input) nodes remain per row
	 * 	5 is nodes uniformly at random deleted within every second row s.t. log(input) nodes remain per row
	 */
	private void deleteNodes(int mode){
		Random randomGenerator = new Random();
		int column;
		numberInnerNodes[mode] = numberInnerNodes[0];
		int row;
		if (mode==1){//1 is nodes structually deleted
			int nodesPerRow = logarithmDualis(2*size);
			int nodesThisRow;
			int distanceThisRow;
			for(int t=0;t<time;t++){
				for(int m=0;m<2*size-1;m++){
					if((innerNodes[0][t][m]!=0) && !(onHorizon(m,t))){
						column = m;
						nodesThisRow = time - Math.abs(pos-column)/2;
						distanceThisRow = nodesThisRow / nodesPerRow +1;
						row = t;
						if(distanceThisRow != 1){
							if((row % distanceThisRow)!=0){
								innerNodes[mode][t][m] = 0;
								numberInnerNodes[mode] -= 2;
							}
						}
					}
				}
			}
		}
		else if (mode==2){//2 is nodes uniformly at random deleted (but the horizon) s.t. log(input) nodes remain
			double aimNodes = logarithmDualis(2*size); //aimed expected value for the number of remaining inner nodes
			double probability= aimNodes/((double) numberInnerNodes[0]); //probability for a inner node not to be deleted s.t. aimNodes inner nodes remain in total 
			double random;
			for(int t=0;t<time;t++){
				for(int m=0;m<2*size-1;m++){
					if((innerNodes[0][t][m]!=0) && !(onHorizon(m,t))){
						random = randomGenerator.nextDouble();
						if(random>probability){
							innerNodes[mode][t][m] = 0;
							numberInnerNodes[mode] -= 2;
						}
					}
				}
			}
		}
		else if (mode==3){//3 is nodes uniformly at random deletes s.t. input*log(input) nodes remain
			double aimNodes = 2*size*logarithmDualis(2*size); //aimed expected value for the number of remaining inner nodes
			double probability= aimNodes/numberInnerNodes[0]; //probability for a inner node not to be deleted s.t. aimNodes inner nodes remain in total 
			double random;
			for(int t=0;t<time;t++){
				for(int m=0;m<2*size-1;m++){
					if((innerNodes[0][t][m]!=0) && !(onHorizon(m,t))){
						random = randomGenerator.nextDouble();
						if(random>probability){
							innerNodes[mode][t][m] = 0;
							numberInnerNodes[mode] -= 2;
						}
					}
				}
			}
		}
		else if (mode==4){//4 in nodes uniformly at random deleted within one row s.t. log(input) nodes remain per row
			double nodesPerRow = logarithmDualis(2*size);
			double nodesThisRow;
			double probability;
			double random;
			for(int t=0;t<time;t++){
				for(int m=0;m<2*size-1;m++){
					if((innerNodes[0][t][m]!=0) && !(onHorizon(m,t))){
						column = m+1;
						nodesThisRow =(double) time - Math.abs(pos-column)/2;
						probability = nodesPerRow/nodesThisRow;
						random = randomGenerator.nextDouble();
						if(random>probability){
							innerNodes[mode][t][m] = 0;
							numberInnerNodes[mode] -= 2;
						}
					}
				}
			}
		}
		else if (mode==5){//5 is nodes uniformly at random deleted within every second row s.t. log(input) nodes remain per row
			double nodesPerRow = logarithmDualis(2*size);
			double nodesThisRow;
			double probability;
			double random;
			for(int t=0;t<time;t++){
				for(int m=1;m<2*size-1;m+=2){
					if((innerNodes[0][t][m]!=0) && !(onHorizon(m,t))){
						column = m+1;
						nodesThisRow = (double) time - Math.abs(pos-column)/2;
						probability = nodesPerRow/nodesThisRow;
						random = randomGenerator.nextDouble();
						if(random>probability){
							innerNodes[mode][t][m] = 0;
							numberInnerNodes[mode] -= 2;
						}
					}
				}
			}
		}
		int nodeCounter = 1;
		for(int t=0;t<time;t++){
			for(int m=0;m<2*size-1;m++){
				if(innerNodes[mode][t][m]!=0){
					innerNodes[mode][t][m]=nodeCounter;
					nodeCounter += 2;
				}
			}
		}
		if (nodeCounter != numberInnerNodes[mode]+1){
			System.out.println("Inner nodes misscounted in mode "+Integer.toString(mode)+", got "+Integer.toString(nodeCounter)+" but need "+Integer.toString(numberInnerNodes[mode]+1));
		}
	}
	
	/*
	 * returns true if the given position is at the time horizon
	 */
	private boolean onHorizon(int m, int t){
		if ((m<pos) && (pos<2*size-2) &&
				((((m % 2)==0) && (t == time-1-(pos-m-1)/2))
					||(((m % 2)!=0)) && (t==time-1-(pos-m)/2))){// on edge of time cone, jump over to adjungiert
			return true;
		}
		else if ((m<pos) && (pos==2*size-2) &&
				((((m % 2)==0) && (t == time-1-(pos-m)/2))
						||(((m % 2)!=0)) && (t==time-1-(pos-m+1)/2))){// on edge of time cone, jump over to adjungiert
			return true;
		}
		else if ((m>pos) &&  (pos>0) &&
				((((m % 2)==0) && (t == time-1-(m-pos-1)/2))
					||(((m % 2)!=0)) && (t==time-1-(m-pos)/2))){// on edge of time cone, jump over to adjungiert
			return true;
		}
		else if ((m>pos) &&  (pos==0) &&
				((((m % 2)==0) && (t == time-1-(m-pos)/2))
						||(((m % 2)!=0)) && (t==time-1-(m-pos+1)/2))){// on edge of time cone, jump over to adjungiert
			return true;
		}
		else if ((m==pos) && (t==time-1)){ // node adjacent to observable
			return true;			
		}
		return false;
	}
	
	/*
	 * returns true if the given position is within the time horizon
	 */
	private boolean withinHorizon(int m, int t){
		if ((m<pos) && (pos<2*size-2) &&
				((((m % 2)==0) && (t <= time-1-(pos-m-1)/2))
					||(((m % 2)!=0)) && (t<=time-1-(pos-m)/2))){// on edge of time cone, jump over to adjungiert
			return true;
		}
		else if ((m<pos) && (pos==2*size-2) &&
				((((m % 2)==0) && (t <= time-1-(pos-m)/2))
						||(((m % 2)!=0)) && (t<=time-1-(pos-m+1)/2))){// on edge of time cone, jump over to adjungiert
			return true;
		}
		else if ((m>pos) &&  (pos>0) &&
				((((m % 2)==0) && (t <= time-1-(m-pos-1)/2))
					||(((m % 2)!=0)) && (t<=time-1-(m-pos)/2))){// on edge of time cone, jump over to adjungiert
			return true;
		}
		else if ((m>pos) &&  (pos==0) &&
				((((m % 2)==0) && (t <= time-1-(m-pos)/2))
						||(((m % 2)!=0)) && (t<=time-1-(m-pos+1)/2))){// on edge of time cone, jump over to adjungiert
			return true;
		}
		else if ((m==pos) && (t<=time-1)){ // node adjacent to observable
			return true;			
		}
		return false;
	}
	
	/*
	 * this function finds the next existing node at an output in direction dir
	 * it outputs the node as an array {time, position, input side} where:
	 * time ranges from 0 to max-1 and from -1 to -max where negative value show the next neighbor is adjungiert
	 * position ranges from 0 to max and -1 is possible meaning that the next neighbor is the observable 
	 * input side == 0 means left
	 */
	private int[] findOutput(int[] node, char dir, int mode){
		int[] output = new int[3];
		int t = node[0];
		int m = node[1];
		if (dir == 'l'){
			output[2] = 1;
		}
		else {
			output[2] = 0;
		}
		if (t>=0){
			if ((m==pos) && (t==time-1)){ // node adjacent to observable
				if (m==0){ // leftmost position
					if (dir == 'l'){ // observable is at this edge
						output[0] = t;
						output[1] = -observable;
						return output;
					}
					else{ // observable is at the other edge, jump over to adjungiert
						output[1] = m;
						output[0] = -t-1;
						output[2] = 1;
						return output;
					}
				}
				else if (m==2*size-2){ // rightmost position
					if (dir == 'r'){ // observable is adjacent at this edge
						output[0] = t;
						output[1] = -observable;
						return output;
					}
					else{ // observable is adjecent to other edge, jump over to adjungiert
						output[1] = m;
						output[0] = -t-1;
						output[2] = 0;
						return output;
					}
				}
				else if (m==observable-1){ // observable is at odd position, meaning left of node
					if (dir=='l'){ // observable is at this edge
						output[0]=t;
						output[1]=-observable;
						return output;
					}
					else{ // observable is at the other edge, jump over to adjungiert
						output[0]=-t-1;
						output[1]=m;
						output[2]=0;
						return output;
					}
				}
				else{ // observable is at even position, meaning right of node
					if (dir=='r'){ // observable is at this edge
						output[0]=t;
						output[1]=-observable;
						return output;
					}
					else{ // observable is at the other edge, jump over to adjungiert
						output[0]=-t-1;
						output[1]=m;
						output[2]=1;
						return output;
					}
				}
			} // node is not adjacent to observable
			else if (dir == 'l'){
				if ((onHorizon(m,t))&&(m<pos)){// on edge of time cone, jump over to adjungiert
							output[1] = m;
							output[0] = -t-1;
							output[2]=0;
							return output;
				}
				else if (m==0){ //leftmost position, going left will jump directly to next timestep
					output[0]=t+1;
					output[1]=m;
					output[2]=0;
					if (innerNodes[mode][output[0]][output[1]]==0){
						return findOutput(output,'l',mode);
					}
					return output;
				}
				else{
					output[1] = m-1;
					if ((m % 2) == 0){ // even position still has neighbor in same timestep
						output[0] = t;
					}
					else{ // odd position jumps to next timestep
						output[0] = t+1;
					}
					if (innerNodes[mode][output[0]][output[1]]==0){
						return findOutput(output,'r',mode);
					}
					return output;
				}
			}
			else if (dir == 'r'){
				if ((onHorizon(m,t))&&(m>pos)){// on edge of time cone, jump over to adjungiert
							output[1] = m;
							output[0] = -t-1;
							output[2]=1;
							return output;
				}
				else if (m==2*size-2){ //rightmost position, going right will jump directly to next timestep
					output[0]=t+1;
					output[1]=2*size-2;
					output[2]=1;
					if (innerNodes[mode][output[0]][output[1]]==0){
						return findOutput(output,'r',mode);
					}
					return output;
				}
				else{
					output[1] = m+1;
					if ((m % 2) == 0){ // even position still has neighbor in same timestep
						output[0] = t;
					}
					else{ // odd position jumps to next timestep
						output[0] = t+1;
					}
					if (innerNodes[mode][output[0]][output[1]]==0){
						return findOutput(output,'l',mode);
					}
					return output;
				}
			}
		}
		else{//input bits
			if((m%2)==0){
				output[0]=0;
				output[1]=m;
				output[2]=1;
				if (innerNodes[mode][output[0]][output[1]]==0){
					return findOutput(output,'l',mode);
				}
				return output;
			}
			else{ //odd position of input
				output[0]=0;
				output[1]=m-1;
				output[2]=0;
				if (innerNodes[mode][output[0]][output[1]]==0){
					return findOutput(output,'r',mode);
				}
				return output;
			}
		}
		return output;
	}
	
	/*
	 * if the graph is sparse this function outputs the line-graph
	 * the output format is specified by the PACE 2017 challenge
	 * graph created by iterating over nodes from original graph, edges alternate from normal time and adjungiert
	 */
	private void transform_linegraph(int mode){
//		if (complete[mode] == false){
		BufferedWriter output = null;
        try {
        	String filename = "timesteps"+Integer.toString(time)+"_inputs"+Integer.toString(2*size)+"_observable"+Integer.toString(observable)+"_mode"+Integer.toString(mode)+"_"+Integer.toString(round[mode]);
            File file = new File("/home/ef/Documents/Uni/Physik/Bachelorarbeit/1dimGraphs/linegraphsPACE/large/" + filename + ".txt");
            file.createNewFile();
            output = new BufferedWriter(new FileWriter(file));
            //create header
            output.write("c line graph of "+Integer.toString(time)+" timesteps, "+Integer.toString(2*size)
            		+" input bits, "+ "observable at position " + Integer.toString(observable) +"\n");
            output.write("c deletion mode "+Integer.toString(mode)+"\n");
            //number of edges in linegraph is 6*(|V|-2n)
            output.write("p tw "+Integer.toString(numberEdges[mode])+" "+Integer.toString(6*numberInnerNodes[mode]+1)+"\n");
            for (int t=0;t<time;t++){
				for (int m=0;m<2*size-1;m++){
					if(innerNodes[mode][t][m]!=0){
						output.write("c line-edges generated by edges at node " + Integer.toString(t) + "," + Integer.toString(m)+"\n");
						for (int in=0;in<4-1;in++){
							for (int out = in+1;out<4;out++){
								output.write(Integer.toString(nodesToEdges[mode][t][m][in]+1) + " " + Integer.toString(nodesToEdges[mode][t][m][out]+1)+"\n");
								output.write(Integer.toString(nodesToEdges[mode][time + t][m][in]+1) + " " + Integer.toString(nodesToEdges[mode][time + t][m][out]+1)+"\n");
							}
						}
						if ((m==pos) && (t==time-1)){ // node adjacent to observable

				            output.write("c edge due to observable \n");
							if (m==0){ // leftmost position
								output.write(Integer.toString(nodesToEdges[mode][t][m][2]+1) + " " + Integer.toString(nodesToEdges[mode][t][m][2]+1+1)+"\n");
							}
							else if (m==2*size-2){ // rightmost position
								output.write(Integer.toString(nodesToEdges[mode][t][m][3]+1) + " " + Integer.toString(nodesToEdges[mode][t][m][3]+1+1)+"\n");
							}
							else if (m==observable-1){ // observable is at odd position, meaning left of node
								output.write(Integer.toString(nodesToEdges[mode][t][m][2]+1) + " " + Integer.toString(nodesToEdges[mode][t][m][2]+1+1)+"\n");
							}
							else{ // observable is at even position, meaning right of node
								output.write(Integer.toString(nodesToEdges[mode][t][m][3]+1) + " " + Integer.toString(nodesToEdges[mode][t][m][3]+1+1)+"\n");
							}
						}
					}
				}
            }
            output.close();
        } catch ( IOException e ) {
            e.printStackTrace();
            System.out.println("you idiot");
        }
//		}
	}
	
	/*
	 * if the graph is sparse this function outputs the line-graph
	 * the output format is specified by the PACE 2017 challenge
	 * graph created by iterating over nodes from original graph, edges alternate from normal time and adjungiert
	 */
	private void transform_graph(int mode){
//		if (complete[mode] == false){
		BufferedWriter output = null;
        try {
        	String filename = "timesteps"+Integer.toString(time)+"_inputs"+Integer.toString(2*size)+"_observable"+Integer.toString(observable)+"_mode"+Integer.toString(mode)+"_"+Integer.toString(round[mode]);
            File file = new File("/home/ef/Documents/Uni/Physik/Bachelorarbeit/1dimGraphs/originalGraphsPACE/large/" + filename + ".txt");
            file.createNewFile();
            output = new BufferedWriter(new FileWriter(file));
            //create header
            output.write("c graph of "+Integer.toString(time)+" timesteps, "+Integer.toString(2*size)
            		+" input bits, "+ "observable at position " + Integer.toString(observable) +"\n");
            output.write("c deletion mode "+Integer.toString(mode)+"\n");
            output.write("p tw "+Integer.toString(numberInnerNodes[mode]+4*size+1)+" "+Integer.toString(numberEdges[mode])+"\n");
            output.write("c input edges"+"\n");
            for (int m=0;m<2*size;m++){
            	output.write(Integer.toString(numberInnerNodes[mode]+1+2*m)+" "+Integer.toString(inputToNodes[mode][m])+"\n");
            	output.write(Integer.toString(numberInnerNodes[mode]+1+2*m+1)+" "+Integer.toString(inputToNodes[mode][2*size+m])+"\n");
            }
            for (int t=0;t<time;t++){
				for (int m=0;m<2*size-1;m++){
					if(innerNodes[mode][t][m]!=0){
						output.write("c output-edges generated by node " + Integer.toString(t) + "," + Integer.toString(m)+"\n");
						for (int out=2;out<4;out++){
							output.write(Integer.toString(innerNodes[mode][t][m])+" "+Integer.toString(nodesToNodes[mode][t][m][out])+"\n");
							if(nodesToNodes[mode][t][m][out]!=innerNodes[mode][t][m]+1){
								if(nodesToNodes[mode][t][m][out]==numberInnerNodes[mode]+4*size+1){
									output.write(Integer.toString(innerNodes[mode][t][m]+1)+" "+Integer.toString(nodesToNodes[mode][t][m][out])+"\n");
								}
								else output.write(Integer.toString(innerNodes[mode][t][m]+1)+" "+Integer.toString(nodesToNodes[mode][t][m][out]+1)+"\n");
							}
						}
					}
				}
            }
            output.close();
        } catch ( IOException e ) {
            e.printStackTrace();
            System.out.println("you idiot");
        }
//		}
	}
	
	/*
	 * if the graph is sparse this function outputs the graph
	 * the output format is
	 * line starting with c is comment
	 * line starting e describes graph, first entry is number of inner Nodes, second entry is number of edges
	 * line containing only of numbers is id edge, time input node, position input node, time output node, position output node
	 */	 
	private void store_graph(int mode){
//		if (complete[mode] == false){
		BufferedWriter output = null;
        try {
        	String filename = "timesteps"+Integer.toString(time)+"_inputs"+Integer.toString(2*size)+"_observable"+Integer.toString(observable)+"_mode"+Integer.toString(mode)+"_"+Integer.toString(round[mode]);
            File file = new File("/home/ef/Documents/Uni/Physik/Bachelorarbeit/1dimGraphs/originalGraphs/large/" + filename + ".txt");
            file.createNewFile();
            output = new BufferedWriter(new FileWriter(file));
            //create header
            output.write("c translation edges to nodes of "+Integer.toString(time)+" timesteps, "+Integer.toString(2*size)
            		+" input bits, "+ "observable at position " + Integer.toString(observable) +"\n");
            output.write("c deletion mode "+Integer.toString(mode)+"\n");
            output.write("e " + Integer.toString(numberInnerNodes[mode]) + " " + Integer.toString(numberEdges[mode])+"\n");
            for(int edge=0;edge<numberEdges[mode];edge++){
            	output.write(Integer.toString(edge + 1) + " " + Integer.toString(edges[mode][edge][0]) + " " + Integer.toString(edges[mode][edge][1]) 
            			+ " " + Integer.toString(edges[mode][edge][2]) + " " + Integer.toString(edges[mode][edge][3]) +"\n");
            }
            output.close();
        } catch ( IOException e ) {
            e.printStackTrace();
            System.out.println("you idiot");
        }
//		}
	}
	
	/*
	 * this function stores the graph in different files. One file for the linegraph as tw input, one for the original graph as tw input and one for the original graph identifying adjacency of edges and nodes
	 * @param startMode first mode for which to store the graph
	 */
	public void store(int startMode){
		for (int mode=startMode;mode<modes;mode++){
			if(stored[mode]==false){
				store_graph(mode);
				transform_graph(mode);
				transform_linegraph(mode);
				stored[mode]=true;
				round[mode]++;
			}
			else{
				System.out.println("Tried to store mode that has already been stored. Mode was: "+Integer.toString(mode));
			}
		}
	}
	
	private int logarithmDualis(int x){
		return (int) Math.round(Math.log((double) x)/Math.log(2.));
	}
}
