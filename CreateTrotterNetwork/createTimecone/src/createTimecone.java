
public class createTimecone {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		for(int i=0;i<15;i++){
			graph g = new graph(10*(i+8),10*(i+8),10*(i+8));
			g.createModes(1);
			g.store(0);
			for (int j=0;j<9;j++){//max should be one smaller than the number of instances one wants to have, since one instance is already generated
				g.createModes(2);
				g.store(2);
			}
		}
	}

}
