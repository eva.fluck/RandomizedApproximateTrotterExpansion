import numpy as np
import scipy.linalg as la
import argparse
import os
import copy as cp
import ncon
import math
import sys

parser = argparse.ArgumentParser(description="compute the contraction given a tree decomposition")
parser.add_argument("file", help="name of the files to read")
parser.add_argument("-t","--time", help="time to simulate in seconds; default=1", type=float, default=1.)
parser.add_argument("-b", "--beta", help="strength of X in hamiltonian; default=0.", type=float, default= 0.)
parser.add_argument("-a", "--alpha", help="; default=0.", type=float, default= 0.)
parser.add_argument("-d", "--disorder",help="if this variable is set the disorder will be read from the given file", default='')
parser.add_argument("-i", "--imaginaryTime", help="switch to imaginary time evolution", action="store_true", default=False)
group3 = parser.add_mutually_exclusive_group()
group3.add_argument("-s","--inputProductState", help="give the input state as string, possible values 0,1,+,-")
group3.add_argument("-S","--inputPosition", help="give the input by position of single particle", type=int, default=0)
parser.add_argument("-H", "--hamiltonian", help="which Hamiltonian do we use;default 0\n0 represents -ZxZ-b/2(1xX+Xx1)\n1 represents d_i*(1xZ+Zx1)+a*(XxX+YxY)+b*ZxZ", type=int, choices=[0,1], default=0)
group = parser.add_mutually_exclusive_group()
group.add_argument("-x", "--observeX", action="store_true", help="set to observe X")
group.add_argument("-y", "--observeY", action="store_true", help="set to observe Y")
group.add_argument("-z", "--observeZ", action="store_true", help="set to observe Z")
group.add_argument("-e", "--observeEye", action="store_true", help="set to observe scalarproduct of input, used to debug; default", default = True)
group2 = parser.add_mutually_exclusive_group()
group2.add_argument("-N", "--none", action="store_true", help="no adjustment for deleted nodes in hamiltonian; default", default = True)
group2.add_argument("-P","--pushHamiltonian", action="store_true", help="adjusting hamiltonian by adding deleted hamiltonian to next in same column")
group2.add_argument("-D","--distributeHamiltonian", action="store_true", help="adjusting hamiltonian by equally distribute deleted hamiltonian within its column")
args = parser.parse_args()

graphPath = '/home/ef553409/git/Physik-Bachelor/1dimGraphs/'
treedecPath = graphPath + 'treeDecLine/heuristic/'
structurePath = graphPath + 'originalGraphs/'
disorderPath = '/home/ef553409/git/Physik-Bachelor/Skripte/'
filename = args.file

values = filename.split('.')[0].split('_')
timeSteps = int(values[0][9:])
inputs = int(values[1][6:])
observable = int(values[2][10:])
mode = values[3][-1]
time = args.time
beta = args.beta
alpha = args.alpha
disorder = args.disorder
inputString = ['0' for i in range(inputs)]
if args.hamiltonian==1:
	disorderArray = [0. for i in range(inputs)]
	if disorder!='':
		disorderFile = open(os.path.join(disorderPath,disorder),'r')
		disorderFile.readline()
		for i in range(inputs):
			disorderArray[i]=float(disorderFile.readline())
		disorderFile.close()

if args.inputPosition !=0:
	if args.inputPosition <= inputs:
		inputString[args.inputPosition -1] = '1'
	else:
		sys.stderr.write("input position out of range, no particle added")
else:
	for i in range(len(args.inputProductState)):
		if i < inputs:
			if args.inputProductState[i] in ['0','1','+','-']:
				inputString[i]=args.inputProductState[i]
			else:
				sys.stderr.write( "at position %i unexpected input %s found, substituted with 0\n" % (i,args.inputProductState[i]))
			
		else:
			sys.stderr.write( "Input String longer than expected, end was cut off\n")
			break
	if inputs>len(args.inputProductState):
		sys.stderr.write( "Input String to short, 0's added\n")

###
# get elimination order
###
eliminationOrder = []
numBags = 0
numOrigEdges = 0
tw=0
bags = {0:0}
bags.clear()
edgeList = []
treeDecFile = open(os.path.join(treedecPath,filename+'.out'),"r")
for line in treeDecFile:
	if line[0]=='c':
		continue
	elif line[0]=='s':
		components = line.split() # line should have layout s td numBags tw+1 numOrigEdges
		numBags = int(components[2])
		tw = int(components[3])-1
		numOrigEdges = int(components[4])
		
	elif line[0]=='b':
		components = line.split() # line should have layout b bagId nodeIds contained in bag
		bags[int(components[1])]=[int(elem) for elem in components[2:]]
		
	else:
		components = line.split() # line should have layout bagId bagId describing an edge
		edgeList.append([int(components[0]),int(components[1])])
treeDecFile.close()
unvisitedNodes = [1]
edges = {0:0}
parent = {1:[]}
edges.clear()
leafs = []

while len(unvisitedNodes)>0:
	currentNode = unvisitedNodes.pop()
	currentChilds = []
	deletionList = []
	for edge in edgeList:
		if edge[0]==currentNode:
			currentChilds.append(edge[1])
			unvisitedNodes.append(edge[1])
			parent[edge[1]]=currentNode
			deletionList.append(edge)
		elif edge[1]==currentNode:
			currentChilds.append(edge[0])
			unvisitedNodes.append(edge[0])
			parent[edge[0]]=currentNode
			deletionList.append(edge)
	for edge in deletionList:
		edgeList.remove(edge)
	edges[currentNode]=currentChilds
	if currentChilds == []:
		leafs.append(currentNode)

if len(edgeList)>0:
	sys.stderr.write('missed edges'+ edgeList)

while len(leafs)>0:
	currentNode = leafs.pop()
	currentBag = bags[currentNode]
	parentBag = bags[parent[currentNode]]
	intersection = list(set(currentBag)&set(parentBag))
	for node in currentBag:
		if node not in intersection:
			eliminationOrder.append(node)
	edges[parent[currentNode]].remove(currentNode)
	if edges[parent[currentNode]]==[] and parent[currentNode]!=1:
		leafs.append(parent[currentNode])
for node in bags[1]:
	eliminationOrder.append(node)

if len(edges[1])>0:
	sys.stderr.write('missed nodes to eliminate'+ edges)
###
# end elimination order
###

###
# read lookup edgeID
###
structureFile = open(os.path.join(structurePath,filename),'r')
edgeID = {0:[]}
for line in structureFile:
	if line[0] == 'c':
		continue
	elif line[0] == 'e':
		continue
	else:
		components = line.split() # line should have layout edgeID inTime inPos outTime outPos
		edgeID[int(components[0])+1]=[int(x) for x in components[1:]] #TODO for new graphs take without +1
structureFile.close()
###
# end read lookup
###




###
# construct tensors and dictionary combining edges with the corresponding tensor indizes
###
def conjugateTensor(tensor):
	if args.imaginaryTime:
		dim = np.ndim(tensor)
		if dim==1:
			return tensor
		elif dim==2:
			return la.inv(tensor)
		elif dim==4:
			matrix = tensor.reshape(4,4)
			return la.inv(matrix).reshape(2,2,2,2)
	else:
		return np.conj(tensor)


Z = np.array([[1.,0.],[0.,-1.]])
Y = np.array([[0.,complex(0.,-1.)],[complex(0.,1.),0.]])
X = np.array([[0.,1.],[1.,0.]])
if args.hamiltonian==0:
	h = -np.kron(Z,Z)-beta/2.*(np.kron(np.eye(2),X)+np.kron(X,np.eye(2)))
elif args.hamiltonian==1:
	h = alpha*(np.kron(X,X)+np.kron(Y,Y))+beta*np.kron(Z,Z)
T = np.array([])
if (not args.distributeHamiltonian) and args.hamiltonian!=1:
	if args.imaginaryTime:
		T_matrix = la.expm((float(time)/float(timeSteps))*h)
	else:
		T_matrix = la.expm(complex(0.,1.)*(float(time)/float(timeSteps))*h)
	T = T_matrix.reshape(2,2,2,2)
zero = np.array([1,0])
one = np.array([0,1])
plus = 1./math.sqrt(2.)*np.array([1,1])
minus = 1./math.sqrt(2.)*np.array([1,-1])
if args.observeX:
	obs = X
elif args.observeY:
	obs = Y
elif args.observeZ:
	obs = Z
elif args.observeEye:
	obs = np.eye(2)

def getInputTensor(inputPostition):
	if inputString[inputPosition]=='0':
		return zero
	elif inputString[inputPosition]=='1':
		return one
	elif inputString[inputPosition]=='+':
		return plus
	elif inputString[inputPosition]=='-':
		return minus

def setInputTensor(nodeID, inputPosition, conjugate = False):
	if conjugate:
		tensors[nodeID]=conjugateTensor(cp.copy(getInputTensor(inputPosition)))
	else:
		tensors[nodeID]=cp.copy(getInputTensor(inputPosition))
	return

def setTensor(nodeID, conjugate = False, push = 0, thisColumnTotal = 0, thisColumnRemaining = 0, tensor=T):
	if args.hamiltonian==1:
		pos = nodeID % inputs
		if pos == 0:
			h_temp = h + disorderArray[0]*(np.kron(np.eye(2),Z)+np.kron(Z,np.eye(2)))
		elif pos == inputs-2:
			h_temp = h + disorderArray[inputs-1]*(np.kron(np.eye(2),Z)+np.kron(Z,np.eye(2)))
		else:
			h_temp = h + disorderArray[pos]*np.kron(Z,np.eye(2)) + disorderArray[pos+1]*np.kron(np.eye(2),Z)
		if args.imaginaryTime:
			T_matrix = la.expm((float(time)/float(timeSteps))*h_temp)
		else:
			T_matrix = la.expm(complex(0.,1.)*(float(time)/float(timeSteps))*h_temp)
		tensor = T_matrix.reshape(2,2,2,2)
	if args.pushHamiltonian:
		T_new = cp.copy(tensor)
		for i in range(push):
			T_new = np.tensordot(T_new,tensor,([2,3],[0,1]))
		if conjugate:
			tensors[nodeID]=cp.copy(conjugateTensor(T_new))
		else:
			tensors[nodeID]=cp.copy(T_new)
	elif args.distributeHamiltonian:
		if args.hamiltonian!=1:
			h_temp=h
		if args.imaginaryTime:
			T_matrix = la.expm((float(time)/float(timeSteps))*(thisColumnTotal/thisColumnRemaining)*h_temp)
		else:
			T_matrix = la.expm(complex(0.,1.)*(float(time)/float(timeSteps))*(thisColumnTotal/thisColumnRemaining)*h_temp)
		T_new = T_matrix.reshape(2,2,2,2)
		if conjugate:
			tensors[nodeID]=cp.copy(conjugateTensor(T_new))
		else:
			tensors[nodeID]=cp.copy(T_new)
	elif args.none:
		if conjugate:
			tensors[nodeID]=cp.copy(conjugateTensor(tensor))
		else:
			tensors[nodeID]=cp.copy(tensor)
	return

def giveInput(inputID, conjugate=False):
	if inputString[inputID]=='0':
		return zero
	elif inputString[inputID]=='1':
		return one
	elif inputString[inputID]=='+':
		return plus
	elif inputString[inputID]=='-':
		return minus

tensors = {0:0}
tensors.clear()
nodeToEdge = {0:0}
nodeToEdge.clear()
resolve = []
resolveDict = {0:[]}
nodes = [[0 for j in range(inputs-1)] for i in range(timeSteps)]
edgeTensorIndizes = {0:[]} #indizes are ordered leftIn rightIn leftOut rightOut

for id in range(numOrigEdges):
	currentEdge = edgeID[id+1]
	###
	# node ID is computed by
	#	time*inputs+position if all positive (time>=timeSteps is adjungiert)
	#	-(postion+1) if input (here position from 0 to 2*inputs-1 possible, >=inputs is adjungiert)
	#	-(2*inputs+1) if observable
	###
	inNodeID=-(3*inputs+1) #absurdly small number
	outNodeID=-(3*inputs+1)
	if (currentEdge[0]>=0 and currentEdge[1]>=0):
		inNodeID=currentEdge[0]*inputs+currentEdge[1]
	elif (currentEdge[0]<0):
		inNodeID=currentEdge[0]*(currentEdge[1]+1)
	else:
		inNodeID=-(2*inputs+1)
	
	# tensor definition for node at input
	if currentEdge[0]==-1:
		if currentEdge[1]>=inputs:
			tensors[inNodeID]=cp.copy(giveInput(currentEdge[1]-inputs,conjugate=True))
		else:
			tensors[inNodeID]=cp.copy(giveInput(currentEdge[1]))
	elif currentEdge[1]==-observable:
		tensors[inNodeID]=cp.copy(obs)
	elif currentEdge[0]<timeSteps:
		nodes[currentEdge[0]][currentEdge[1]] = 1
	
	if inNodeID in nodeToEdge:
		nodeToEdge[inNodeID].append(id+1)
	else:
		nodeToEdge[inNodeID]=[id+1]
	
	
	if (currentEdge[2]>=0 and currentEdge[3]>=0):
		outNodeID=currentEdge[2]*inputs+currentEdge[3]
	else:
		outNodeID=-(2*inputs+1)
	
	# tensor definition for node at output
	if currentEdge[2]==-1:
		if currentEdge[3]>=inputs:
			tensors[outNodeID]=cp.copy(giveInput(currentEdge[3]-inputs,conjugate=True)) 
		else:
			tensors[outNodeID]=cp.copy(giveInput(currentEdge[3]))
	elif currentEdge[3]==-observable:
		tensors[outNodeID]=cp.copy(obs)
	elif currentEdge[2]<timeSteps:
		nodes[currentEdge[2]][currentEdge[3]] = 1
	
	if outNodeID in nodeToEdge:
		nodeToEdge[outNodeID].append(id+1)
	else:
		nodeToEdge[outNodeID]=[id+1]
	
	# update edgeID to edgeTensorIndizes
	if (currentEdge[1]==-observable):
		edgeTensorIndizes[id+1]=[inNodeID,1,outNodeID,5]
		if observable == 1:
			edgeTensorIndizes[id+1][3]=2
		elif observable == inputs:
			edgeTensorIndizes[id+1][3]=3
		elif observable%2==0:
			edgeTensorIndizes[id+1][3]=2
		else:
			edgeTensorIndizes[id+1][3]=3
	elif (currentEdge[3]==-observable):
		edgeTensorIndizes[id+1]=[inNodeID,5,outNodeID,0]
		if observable == 1:
			edgeTensorIndizes[id+1][1]=2
		elif observable == inputs:
			edgeTensorIndizes[id+1][1]=3
		elif observable%2==0:
			edgeTensorIndizes[id+1][1]=2
		else:
			edgeTensorIndizes[id+1][1]=3
	elif (currentEdge[0]==-1):
		edgeTensorIndizes[id+1]=[inNodeID,0,outNodeID,5]
		if (currentEdge[1]%2==0) and (currentEdge[3]%2==0):
			edgeTensorIndizes[id+1][3]=0
		elif (currentEdge[1]%2==1) and (currentEdge[3]%2==0):
			edgeTensorIndizes[id+1][3]=1
		elif (currentEdge[1]%2==0) and (currentEdge[3]%2==1):
			edgeTensorIndizes[id+1][3]=1
		elif (currentEdge[1]%2==1) and (currentEdge[3]%2==1):
			edgeTensorIndizes[id+1][3]=0
	elif (currentEdge[1]<currentEdge[3]):
		edgeTensorIndizes[id+1]=[inNodeID,3,outNodeID,0]
	elif (currentEdge[1]>currentEdge[3]):
		edgeTensorIndizes[id+1]=[inNodeID,2,outNodeID,1]
	else: #edge goes to a node directly above, direction unknown or jump to adjungiert
		resolve.append(id+1)
		resolveDict[id+1]=[inNodeID,outNodeID,currentEdge]

while len(resolve)>0:
	id = resolve.pop()
	currentResolve = resolveDict[id]
	inNodeID = currentResolve[0]
	outNodeID = currentResolve[1]
	inNeighbors = nodeToEdge[inNodeID]
	inIndizes = []
	edge = currentResolve[2]
	edgeTensorIndizes[id]=[inNodeID,5,outNodeID,5]
	for neighbor in inNeighbors:
		if neighbor==id:
			continue
		elif (neighbor not in resolveDict) and edgeTensorIndizes[neighbor][0]==inNodeID:
			inIndizes.append(edgeTensorIndizes[neighbor][1])
		elif (neighbor not in resolveDict) and edgeTensorIndizes[neighbor][2]==inNodeID:
			inIndizes.append(edgeTensorIndizes[neighbor][3])
	
	if (edge[0]+timeSteps == edge[2]): #jump adjungiert
		if 2 in inIndizes:
			edgeTensorIndizes[id][1]=3
			edgeTensorIndizes[id][3]=3
		else:
			edgeTensorIndizes[id][1]=2
			edgeTensorIndizes[id][3]=2
	else:
		if 2 in inIndizes:
			edgeTensorIndizes[id][1]=3
			edgeTensorIndizes[id][3]=1
		else:
			edgeTensorIndizes[id][1]=2
			edgeTensorIndizes[id][3]=0
	del resolveDict[id]

###
# end construct tensors
###

for m in range(inputs-1):
	countToNext=0
	totalCounter=0
	for t in range(timeSteps):
		if nodes[t][m]!=0:
			nodes[t][m]+=countToNext
			totalCounter+=1
			if args.pushHamiltonian:
				setTensor(t*inputs+m,push=countToNext)
				setTensor((t+timeSteps)*inputs+m, conjugate=True,push=countToNext)
				countToNext=0
			elif args.distributeHamiltonian:
				countToNext+=1
			elif args.none:
				setTensor(t*inputs+m)
				setTensor((t+timeSteps)*inputs+m, conjugate=True)
if args.distributeHamiltonian:
	for m in range(inputs-1):
		for t in range(timeSteps):
			if nodes[t][m]!=0:
				setTensor(t*inputs+m,thisColumnTotal=countToNext,thisColumnRemaining=totalCounter)
				setTensor((t+timeSteps)*inputs+m, conjugate=True,thisColumnTotal=countToNext,thisColumnRemaining=totalCounter)

###
# tensor contraction
###
for i in range(numOrigEdges):
	#for j in range(numOrigEdges+1):
		#if j>0 and ((edgeTensorIndizes[j][1] < 0) or (edgeTensorIndizes[j][3] < 0)):
			#print j , edgeTensorIndizes[j]
	currentEdgeID = eliminationOrder[i]
	target = edgeTensorIndizes[currentEdgeID]
	if target[0]!=target[2]:
		newTensor = min(target[0],target[2])
		newTensor = min(target[0],target[2])
		sizeTarget0 = np.ndim(tensors[target[0]])
		tensors[newTensor]=np.tensordot(tensors[target[0]],tensors[target[2]],(target[1],target[3]))
		inNeighbors = nodeToEdge[target[0]]
		inNeighbors.remove(currentEdgeID)
		outNeighbors = nodeToEdge[target[2]]
		outNeighbors.remove(currentEdgeID)
		changeTensor = []
		if len(inNeighbors)>0:
			for id in inNeighbors:
				tensorIndizes = edgeTensorIndizes[id]
				if tensorIndizes[0]==target[0]:
					changeTensor.append([id,0])
					index = tensorIndizes[1]
					if index>target[1]:
						edgeTensorIndizes[id][1] -= 1
				if tensorIndizes[2]==target[0]:
					changeTensor.append([id,2])
					index = tensorIndizes[3]
					if index>target[1]:
						edgeTensorIndizes[id][3] -= 1
		if len(outNeighbors)>0:
			for id in outNeighbors:
				tensorIndizes = edgeTensorIndizes[id]
				if tensorIndizes[0]==target[2]:
					changeTensor.append([id,0])
					index = tensorIndizes[1]
					if index>target[3]:
						edgeTensorIndizes[id][1] = sizeTarget0-1+index-1
					else:
						edgeTensorIndizes[id][1] = sizeTarget0-1+index
				if tensorIndizes[2]==target[2]:
					changeTensor.append([id,2])
					index = tensorIndizes[3]
					if index>target[3]:
						edgeTensorIndizes[id][3] = sizeTarget0-1+index-1
					else:
						edgeTensorIndizes[id][3] = sizeTarget0-1+index
		nodeToEdge[newTensor]=list(set(inNeighbors)|set(outNeighbors))
		while len(changeTensor)>0:
			currentChange = changeTensor.pop()
			edgeTensorIndizes[currentChange[0]][currentChange[1]]=newTensor
		#print tensors[newTensor]
	else:
		#print target
		contract=[[0 for i in range(np.ndim(tensors[target[0]]))]]
		contract[0][target[1]]=1
		contract[0][target[3]]=1
		neighbors = nodeToEdge[target[0]]
		neighbors.remove(currentEdgeID)
		for i in range(np.ndim(tensors[target[0]])):
			#print i, target[1], target[3]
			if (i!=target[1])and(i!=target[3]):
				contract[0][i]=-(i+1)
				if (i>target[1]):
					contract[0][i]+=1
				if (i>target[3]):
					contract[0][i]+=1
		#print contract
		if len(neighbors)>0:
			for id in neighbors:
				tensorIndizes = edgeTensorIndizes[id]
				if tensorIndizes[0]==target[0]:
					edgeTensorIndizes[id][0] = target[0]
					edgeTensorIndizes[id][1] = (-contract[0][edgeTensorIndizes[id][1]])-1
				if tensorIndizes[2]==target[0]:
					edgeTensorIndizes[id][2] = target[0]
					edgeTensorIndizes[id][3] = (-contract[0][edgeTensorIndizes[id][3]])-1
		tensors[target[0]]=ncon.ncon(tensors[target[0]],contract)
		nodeToEdge[target[0]]=list(set(neighbors))
		#print tensors[target[0]]
	#raw_input("Press Enter to continue...")
###
# end tensor contraction
###

sys.stdout.write("observable: {0.real} +  {0.imag} i\n".format(tensors[-(2*inputs+1)])) #result found in tensor of smallest possible index
