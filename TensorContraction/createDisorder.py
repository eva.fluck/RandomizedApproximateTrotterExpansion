### input is max value of disorder

import math
import sys
import random as rand

disorderFileName = './disorder{}.txt'.format(rand.randint(1,9999))
sys.stdout.write(disorderFileName.split('/')[1].split('.')[0])
disorder = float(sys.argv[1])

disorderFile = open(disorderFileName,'w')
disorderFile.write('max disorder {}\n'.format(disorder))
for i in range(20):
	disorderFile.write(str(rand.uniform(-disorder,disorder))+'\n')
disorderFile.close()
